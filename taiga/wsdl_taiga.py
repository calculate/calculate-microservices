# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import sys

from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.core.server.func import WsdlBase
from .taiga import Taiga
from .utils.cl_taiga import ClTaigaCreateUser, ClTaigaDeleteUser, ClTaigaUpdateData, ClTaigaCreateTemplate, \
    ClTaigaTestTable, ClTaigaDeleteFromTable, ClTaigaChangeTable, ClTaigaUserTable, ClTaigaChangeUser


_ = lambda x: x
setLocalTranslate('cl_update3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Wsdl(WsdlBase):
    methods = [
        {
            # идентификатор метода
            'method_name': "create_user",
            # категория метода
            'category': __('Taiga'),
            # заголовок метода
            'title': __("Create taiga user"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-create-user',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaCreateUser,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Create user"),
                                    normal=('cl_taiga_user_login',
                                            'cl_taiga_user_template',
                                            'cl_taiga_add_roles'),
                                    next_label=_("Save"))]
        },
        {
            # идентификатор метода
            'method_name': "delete_users",
            # категория метода
            'category': __('Taiga'),
            # заголовок метода
            'title': __("Delete taiga user"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-delete-user',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaDeleteUser,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Show user"),
                                    normal=('cl_taiga_users_login',),
                                    next_label=_("Save"))]
        },
        {
            # идентификатор метода
            'method_name': "update_data",
            # категория метода
            'category': __('Taiga'),
            # заголовок метода
            'title': __("Update taiga data"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-update',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaUpdateData,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': []
        },
        {
            # идентификатор метода
            'method_name': "create_template",
            # категория метода
            #'category': __('Taiga'),
            # заголовок метода
            'title': __("Create taiga template"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-create-template',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaCreateTemplate,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Show user"),
                                    normal=('cl_taiga_template_name',
                                            'cl_taiga_role_list',),
                                    next_label=_("Save"))]
        },
        {
            # идентификатор метода
            'method_name': "role_table",
            # категория метода
            'category': __('Taiga'),
            # заголовок метода
            'title': __("show taiga table"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-test-table',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaTestTable,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': []
        },
        {
            # идентификатор метода
            'method_name': "detailed_role_table",
            # категория метода
            #'category': __('Taiga'),
            # заголовок метода
            'title': __("detailed taiga table"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            #'command': 'cl-taiga-test-table',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {},
            # описание действия
            'action': None,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Table details"),
                                    normal=(
                                        'cl_taiga_templates_names',
                                        'cl_taiga_templates_roles'),
                                    custom_buttons=[('but0', _("Back"),
                                                     "role_table",
                                                     "button"),
                                                    ('but1', _("Delete"),
                                                     "delete_template_table",
                                                     "button"),
                                                    ('but2', _("Change"),
                                                     "change_template_table",
                                                     "button")
                                                    ])
            ]
        },
        {
            # идентификатор метода
            'method_name': "delete_template_table",
            # категория метода
            #'category': __('Taiga'),
            # заголовок метода
            'title': __("delete taiga table"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            # 'command': 'cl-taiga-test-table',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaDeleteFromTable,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Table details"),
                                    normal=(
                                        'cl_taiga_templates_names',
                                        'cl_taiga_templates_roles'))
            ]
        },
        {
            # идентификатор метода
            'method_name': "change_template_table",
            # категория метода
            # 'category': __('Taiga'),
            # заголовок метода
            'title': __("change taiga table"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            # 'command': 'cl-taiga-test-table',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaChangeTable,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Table details"),
                                    normal=(
                                        'cl_taiga_templates_names',
                                        'cl_taiga_templates_roles'))
            ]
        },
        {
            # идентификатор метода
            'method_name': "user_table",
            # категория метода
            'category': __('Taiga'),
            # заголовок метода
            'title': __("show user table"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-user-table',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaUserTable,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': []
        },
        {
            # идентификатор метода
            'method_name': "change_user",
            # категория метода
            'category': __('Taiga'),
            # заголовок метода
            'title': __("change user"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-taiga-change-user',
            # права для запуска метода
            'rights': ['taiga'],
            # объект содержащий модули для действия
            'logic': {'Taiga': Taiga},
            # описание действия
            'action': ClTaigaChangeUser,
            # объект переменных
            'datavars': "taiga",
            'native_error': (),
            # значения по умолчанию для переменных этого метода
            'setvars': {},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Table details"),
                                    normal=(
                                        'cl_taiga_change_login',
                                        'cl_taiga_add_roles'),
                                    next_label=_("Save"))
            ]
        },
    ]

