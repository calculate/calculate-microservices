import requests
import json
from calculate.core.server.func import MethodsInterface
from .datavars import DataVarsTaiga


class TaigaConnectionError(Exception):
    "Failed to connect to taiga"


class Taiga(MethodsInterface):
    def test(self):
        return True

    def check_connection(self, server_url):
        #try:
        resp = requests.get(server_url)
        if resp.status_code in range(200, 300):
            self.printSUCCESS("Connected to taiga")
            return True
        #except:
            #raise TaigaConnectionError('Connection failed')

    def create_user(self, server_url):
        var = self.clVars.Get
        resp = requests.post(f"{server_url}/service_taiga/create_user", json={"login": var('cl_taiga_user_login'),
                                                                              "template": var('cl_taiga_user_template'),
                                                                              "add_roles": var('cl_taiga_add_roles')})
        if resp.status_code in range(200, 300):
            self.printSUCCESS("Connected to taiga")
            return True
        raise TaigaConnectionError(json.loads(resp.text)['status'])

    def delete_user(self, server_url):
        users = self.clVars.Get('cl_taiga_users_login')
        resp = requests.post(f"{server_url}/service_taiga/delete_user", json={'users': users})
        return True

    def change_user(self, server_url):
        var = self.clVars.Get
        user = var()

    def create_template(self, server_url):
        var = self.clVars.Get
        name = var('cl_taiga_template_name')
        roles = var('cl_taiga_role_list')
        resp = requests.post(f"{server_url}/service_taiga/create_template", json={'name': name,
                                                                                  "roles": roles})
        return True

    def show_template_table(self, server_url):
        var = self.clVars.Get
        templates = var('cl_taiga_all_templates')
        templates = [(x, ', '.join(y)) for x, y in templates]
        self.printTable('test', ['template name', 'template roles'], templates,
                        fields=['cl_taiga_templates_names', ''],
                        onClick="detailed_role_table",
                        addAction='create_template')
        return True

    def delete_template(self, server_url):
        var = self.clVars.Get
        template_name = var('cl_taiga_templates_names')
        resp = requests.post(f"{server_url}/service_taiga/delete_template", json={"template": template_name})
        return True

    def update_data(self, server_url):
        upd_projects = requests.get(f"{server_url}/service_taiga/update_projects")
        upd_roles = requests.get(f"{server_url}/service_taiga/update_roles")
        upd_users = requests.get(f"{server_url}/service_taiga/update_users")
        return True

    def change_template(self, server_url):
        var = self.clVars.Get
        template_name = var('cl_taiga_templates_names')
        template_roles = var('cl_taiga_templates_roles')
        resp = requests.post(f"{server_url}/service_taiga/change_template",
                             json={"template": template_name, "roles": template_roles})
        return True

    def show_user_table(self, server_url):
        var = self.clVars.Get
        users = [(x['full_name'], x['template'], x['extra_projects']) for x in var('cl_taiga_all_users')]
        self.printTable('users', ['fio', 'user template', 'additional projects'], sorted(users),
                        fields=['cl_taiga_add_roles', ''],
                        addAction='create_user',
                        onClick='change_user')
        return True

    def change_user(self, server_url):
        var = self.clVars.Get
        user = var('cl_taiga_change_login')
        roles = var('cl_taiga_add_roles')
        resp = requests.post(f"{server_url}/service_taiga/change_user", json={"login": user, "roles": roles})
        return True

