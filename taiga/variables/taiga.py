from calculate.lib.datavars import Variable, ReadonlyVariable, VariableError
import requests
import json


class VariableClTaigaServerUrl(Variable):
    type = 'choiceedit'
    metavalue = 'URL'
    value = 'http://0.0.0.0:41324'

    def init(self):
        self.label = 'Server url'
        self.help = 'which url connect to'

    def get(self):
        return self.value


class VariableClTaigaUserLogin(Variable):
    type = 'choiceedit'
    metavalue = 'LOGIN'
    opt = ['-l']
    untrusted = True

    def init(self):
        self.label = "User login"
        self.help = "user login"


class VariableClTaigaAllUsers(Variable):
    type = 'list'

    def get(self):
        resp = requests.get(f"{self.Get('cl_taiga_server_url')}/service_taiga/users")
        return resp.json()['data']


class VariableClTaigaUsersLogin(Variable):
    users = 'cl_taiga_all_users'
    type = 'choiceedit-list'
    metavalue = 'ROLE'
    opt = ['-q']
    untrusted = True

    def init(self):
        self.label = "Userqwe role"
        self.help = "userqwe role on project"

    def check(self, value):
        if not value:
            raise ValueError('Can not be empty')

    def choice(self):
        resp = self.Get(self.users)
        return [str(x['login']) for x in resp]



class VariableClTaigaProjectName(Variable):
    type = 'string'
    metavalue = 'NAME'
    opt = ['-n']

    def init(self):
        self.label = 'Project name'
        self.help = 'Name for a new project'

    def check(self, value):
        resp = requests.get(f"{self.Get('cl_taiga_server_url')}/service_taiga/projects")
        if value in [str(x['name']) for x in resp.json()['data']]:
            raise VariableError("Проект с таким именем уже существует")


class VariableClTaigaRoleRel(Variable):
    type = 'choiceedit-list'
    metavalue = 'ROLE'

    def init(self):
        self.label = "User role"
        self.help = "Roles user should be added to"

    def choice(self):
        resp = requests.get(f"{self.Get('cl_taiga_server_url')}/service_taiga/roles")
        result = []
        a = resp.json()['data']
        for key, value in resp.json()['data'].items():
            for role in value:
                if role['name'] != "AUTO":
                    result.append(f"{key} - {role['name']}")
        return result


class VariableClTaigaRoleList(Variable):
    type = 'choiceedit-list'
    metavalue = 'ROLE'
    roles = 'cl_taiga_role_rel'

    def init(self):
        self.label = "User roles"
        self.help = "Roles user should be added to"

    def choice(self):
        return self.Choice(self.roles)


class VariableClTaigaTemplateName(Variable):
    type = 'string'
    metavalue = 'NAME'
    untrusted = True

    def init(self):
        self.label = "Template name"
        self.help = "Template name"

    def check(self, value):
        if not value:
            raise VariableError("Can not be empty")


class VariableClTaigaAllTemplates(Variable):
    type = 'list'

    def get(self):
        resp = requests.get(f"{self.Get('cl_taiga_server_url')}/service_taiga/templates")
        return resp.json()["data"]


class VariableClTaigaTemplatesNames(Variable):
    type = 'string'

    def init(self):
        self.label = "Template name"
        self.help = "Template name"

    def get(self):
        res = self.Get('cl_taiga_all_templates')
        return [x for x in res]


class VariableClTaigaTemplatesRoles(Variable):
    type = 'choice-list'

    def init(self):
        self.label = "Template role"
        self.help = "Template role"

    def get(self):
        name = self.Get('cl_taiga_templates_names')
        res = self.Get('cl_taiga_all_templates')
        return [x[1] for x in res if x[0] == name][0]

    def choice(self):
        return self.Choice('cl_taiga_role_rel')

    def check(self, value):
        if not value:
            raise VariableError("Can not be empty")


class VariableClTaigaUserTemplate(Variable):
    type = 'choice'
    metavalue = 'TEMPLATE'
    opt = ['-t']
    template = 'cl_taiga_all_templates'

    def init(self):
        self.label = "User template"
        self.help = "template to apply on user"

    def choice(self):
        return [x[0] for x in self.Get(self.template)]

class VariableClTaigaAddRoles(Variable):
    type = 'choice-list'
    metavalue = 'ROLE'
    untrusted = True
    opt = ['-r']

    def init(self):
        self.label = "User role"
        self.help = "Roles user should be added to"

    def get(self):
        return [self.value]

    def choice(self):
        resp = requests.get(f"{self.Get('cl_taiga_server_url')}/service_taiga/roles")
        result = []
        a = resp.json()['data']
        for key, value in resp.json()['data'].items():
            for role in value:
                if role['name'] != "AUTO":
                    result.append(f"{key} - {role['name']}")
        return result


class VariableClTaigaChangeLogin(Variable):
    type = 'choiceedit'

    def init(self):
        self.label = "User role"
        self.help = "Roles user should be added to"


class VariableClTaigaChangeRoles(Variable):
    type = 'choiceedit-list'

    def init(self):
        self.label = "Users projects"
        self.help = "Choose new projects for user"

