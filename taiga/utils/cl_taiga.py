import sys

from calculate.core.server.func import Action, Tasks, AllTasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from ..taiga import TaigaConnectionError


_ = lambda x: x
setLocalTranslate('cl_update3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClTaigaCreateUser(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = "Created"
    failedMessage = "Not created"
    interruptMessage = __("Update manually interrupted")

    tasks = [
        # {'name': 'connection',
        #  'method': 'Taiga.check_connection(cl_taiga_server_url)',
        #  'message': 'Checking connection'},
        {'name': 'sending_request',
         'method': 'Taiga.create_user(cl_taiga_server_url)',
         'message': 'Creating user-add task'}
    ]


class ClTaigaDeleteUser(Action):
    """
        Действие обновление конфигурационных файлов
        """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = None
    failedMessage = None
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'connection',
         'method': 'Taiga.check_connection(cl_taiga_server_url)',
         'message': 'Checking connection'},
        {'name': 'sending_request',
         'method': 'Taiga.delete_user(cl_taiga_server_url)',
         'message': 'Creating user-del task'}
    ]


class ClTaigaUpdateData(Action):
    """
        Действие обновление конфигурационных файлов
        """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = None
    failedMessage = None
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'connection',
         'method': 'Taiga.check_connection(cl_taiga_server_url)',
         'message': 'Checking connection'},
        {'name': 'sending_request',
         'method': 'Taiga.update_data(cl_taiga_server_url)',
         'message': 'Creating update task'}
    ]

class ClTaigaCreateTemplate(Action):
    """
        Действие обновление конфигурационных файлов
        """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = None
    failedMessage = None
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'connection',
         'method': 'Taiga.check_connection(cl_taiga_server_url)',
         'message': 'Checking connection'},
        {'name': 'sending_request',
         'method': 'Taiga.create_template(cl_taiga_server_url)',
         'message': 'Creating update task'}
    ]

class ClTaigaTestTable(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = "Created"
    failedMessage = "Not created"
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'view_table',
         'method': 'Taiga.show_template_table(cl_taiga_server_url)'}
    ]

class ClTaigaDeleteFromTable(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = "Created"
    failedMessage = "Not created"
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'connection',
         'method': 'Taiga.check_connection(cl_taiga_server_url)',
         'message': 'Checking connection'},
        {'name': 'sending_request',
         'method': 'Taiga.delete_template(cl_taiga_server_url)',
         'message': 'Creating delete task'}
    ]

class ClTaigaChangeTable(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = "Created"
    failedMessage = "Not created"
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'connection',
         'method': 'Taiga.check_connection(cl_taiga_server_url)',
         'message': 'Checking connection'},
        {'name': 'sending_request',
         'method': 'Taiga.change_template(cl_taiga_server_url)',
         'message': 'Creating delete task'}
    ]

class ClTaigaUserTable(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (TaigaConnectionError)

    successMessage = "Created"
    failedMessage = "Not created"
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'view_table',
         'method': 'Taiga.show_user_table(cl_taiga_server_url)'}
    ]

class ClTaigaChangeUser(Action):
    """Изменение данных пользователя"""
    native_error = (TaigaConnectionError)

    successMessage = "Created"
    failedMessage = "Not created"
    interruptMessage = __("Update manually interrupted")

    tasks = [
        {'name': 'change_user',
         'method': 'Taiga.change_user(cl_taiga_server_url)'}
    ]