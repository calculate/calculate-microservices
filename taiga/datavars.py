__app__ = 'calculate-taiga'
__version__ = '3.4.1'

import sys
from calculate.lib.datavars import DataVars

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_update3', sys.modules[__name__])


class DataVarsTaiga(DataVars):
    """Variable class for installation"""

    def importInstall(self, **args):
        """Import install variables"""
        self.importVariables()
        self.importVariables('calculate.taiga.variables')
        self.defaultModule = "taiga"